
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.sample.orderprocessing.BusinessRulesEngineApplication;
import com.sample.orderprocessing.db.RulesRepository;
import com.sample.orderprocessing.models.OrderProcessingRequest;
import com.sample.orderprocessing.models.OrderProcessingResponse;
import com.sample.orderprocessing.models.OrderProcessingRule;
import com.sample.orderprocessing.service.OrderRulesService;
import com.sample.orderprocessing.util.PaymentType;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BusinessRulesEngineApplication.class)
class RuleEngineTest {
	
    @InjectMocks
    private OrderRulesService orderRulesServiceMock;
    
    @Mock
    private RulesRepository rulesRepoMock;
    
	@Test
    public void verifyGetAllRules() throws Exception {
        Mockito.when(rulesRepoMock.findAll()).thenReturn(getListOfRules());
        List<OrderProcessingRule> allRules = orderRulesServiceMock.getAllRules();
        assertEquals(allRules.size(), 9);
    }
	
	@Test
    public void verifyAddNewRule() throws Exception {
        Mockito.when(rulesRepoMock.save(Mockito.any())).thenReturn(new OrderProcessingRule("testProdType","testRule"));
        OrderProcessingRequest newRequest = new OrderProcessingRequest("testProdType", "testRule");
        OrderProcessingRule newRule = orderRulesServiceMock.postAddNewRule(newRequest);
        assertEquals(newRule.getProductType(), "testProdType");
        assertEquals(newRule.getOperationRule(), "testRule");
    }
	
	@Test
    public void verifyOrderProcessOfBook() throws Exception {
        Mockito.when(rulesRepoMock.findByProductType(Mockito.anyString())).thenReturn(getListOfRules().stream().filter(rule->rule.getProductType().equalsIgnoreCase(PaymentType.BOOK.toString())).collect(Collectors.toList()));
        OrderProcessingRequest request = new OrderProcessingRequest("payment", "book");
        List<OrderProcessingResponse> allRules = orderRulesServiceMock.processOrder(request);
        assertEquals(allRules.size(), 2);
    }
	
	@Test
    public void verifyOrderProcessOfPhysical() throws Exception {
        Mockito.when(rulesRepoMock.findByProductType(Mockito.anyString())).thenReturn(getListOfRules().stream().filter(rule->rule.getProductType().equalsIgnoreCase(PaymentType.PHYSICAL.toString())).collect(Collectors.toList()));
        OrderProcessingRequest request = new OrderProcessingRequest("payment", "PHYSICAL");
        List<OrderProcessingResponse> allRules = orderRulesServiceMock.processOrder(request);
        assertEquals(allRules.size(), 2);
    }
	
	@Test
    public void verifyOrderProcessOfMembership() throws Exception {
        Mockito.when(rulesRepoMock.findByProductType(Mockito.anyString())).thenReturn(getListOfRules().stream().filter(rule->rule.getProductType().equalsIgnoreCase(PaymentType.MEMBERSHIP.toString())).collect(Collectors.toList()));
        OrderProcessingRequest request = new OrderProcessingRequest("payment", "MEMBERSHIP");
        List<OrderProcessingResponse> allRules = orderRulesServiceMock.processOrder(request);
        assertEquals(allRules.size(), 2);
    }
	
	@Test
    public void verifyOrderProcessOfUpgrade() throws Exception {
        Mockito.when(rulesRepoMock.findByProductType(Mockito.anyString())).thenReturn(getListOfRules().stream().filter(rule->rule.getProductType().equalsIgnoreCase(PaymentType.UPGRADE.toString())).collect(Collectors.toList()));
        OrderProcessingRequest request = new OrderProcessingRequest("payment", "UPGRADE");
        List<OrderProcessingResponse> allRules = orderRulesServiceMock.processOrder(request);
        assertEquals(allRules.size(), 2);
    }
	
	@Test
    public void verifyOrderProcessOfVideo() throws Exception {
        Mockito.when(rulesRepoMock.findByProductType(Mockito.anyString())).thenReturn(getListOfRules().stream().filter(rule->rule.getProductType().equalsIgnoreCase(PaymentType.VIDEO.toString())).collect(Collectors.toList()));
        OrderProcessingRequest request = new OrderProcessingRequest("payment", "VIDEO");
        List<OrderProcessingResponse> allRules = orderRulesServiceMock.processOrder(request);
        assertEquals(allRules.size(), 1);
    }

	private List<OrderProcessingRule> getListOfRules() {
		List<OrderProcessingRule> rules = new ArrayList<OrderProcessingRule>();
		rules.add(new OrderProcessingRule("PHYSICAL","Generate packing slip for shipping"));
		rules.add(new OrderProcessingRule("BOOK","Create duplicate slip for shipping"));
		rules.add(new OrderProcessingRule("PHYSICAL","Generate commission payment to agent"));
		rules.add(new OrderProcessingRule("BOOK","Generate commission payment to agent"));
		rules.add(new OrderProcessingRule("MEMBERSHIP","Activate membership"));
		rules.add(new OrderProcessingRule("UPGRADE","Apply upgrade"));
		rules.add(new OrderProcessingRule("MEMBERSHIP","Email owner"));
		rules.add(new OrderProcessingRule("UPGRADE","Email owner"));
		rules.add(new OrderProcessingRule("VIDEO","Add free first aid video to packing slip"));
		return rules;
	}
}
