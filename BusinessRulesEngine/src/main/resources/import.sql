insert into ORDER_PROCESSING_RULE (id_order_processing,product_type,operation_rule) values (11,'PHYSICAL','Generate packing slip for shipping');
insert into ORDER_PROCESSING_RULE (id_order_processing,product_type,operation_rule) values (22,'BOOK','Create duplicate slip for shipping');
insert into ORDER_PROCESSING_RULE (id_order_processing,product_type,operation_rule) values (33,'PHYSICAL','Generate commission payment to agent');
insert into ORDER_PROCESSING_RULE (id_order_processing,product_type,operation_rule) values (44,'BOOK','Generate commission payment to agent');
insert into ORDER_PROCESSING_RULE (id_order_processing,product_type,operation_rule) values (55,'MEMBERSHIP','Activate membership');
insert into ORDER_PROCESSING_RULE (id_order_processing,product_type,operation_rule) values (66,'UPGRADE','Apply upgrade');
insert into ORDER_PROCESSING_RULE (id_order_processing,product_type,operation_rule) values (77,'MEMBERSHIP','Email owner');
insert into ORDER_PROCESSING_RULE (id_order_processing,product_type,operation_rule) values (88,'UPGRADE','Email owner');
insert into ORDER_PROCESSING_RULE (id_order_processing,product_type,operation_rule) values (99,'VIDEO','Add free first aid video to packing slip');
