package com.sample.orderprocessing.models;

public class OrderProcessingResponse {
	
	String processingOperation; 
	String productType;
	
	public OrderProcessingResponse()
	{}

	public OrderProcessingResponse(String productType, String operationRule) {
		this.processingOperation = operationRule;
		this.productType = productType;
	}

	public String getProcessingOperation() {
		return processingOperation;
	}

	public void setProcessingOperation(String processingOperation) {
		this.processingOperation = processingOperation;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}
	
	
}
