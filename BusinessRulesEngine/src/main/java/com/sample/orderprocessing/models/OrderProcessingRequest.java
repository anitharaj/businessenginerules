package com.sample.orderprocessing.models;

import java.io.Serializable;

public class OrderProcessingRequest implements Serializable{

	private static final long serialVersionUID = 3581827597315676877L;

	private String operation; //payment
	private String productType;// book or anytype
	
	public OrderProcessingRequest() {
		super();
	}

	public OrderProcessingRequest(String operation, String productType) {
		this.operation = operation;
		this.productType = productType;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}
	
	
	
}
