package com.sample.orderprocessing.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ORDER_PROCESSING_RULE")
public class OrderProcessingRule {
	
	@Id
	@GeneratedValue
	@Column(name = "id_order_processing")
	private int ruleId;
	
	@Column(name = "product_type")
	private String productType;

	@Column(name = "operation_rule")
	private String operationRule;
	
	public OrderProcessingRule() {}
	
	public OrderProcessingRule(String productType, String operationRule) {
		this.productType = productType;
		this.operationRule = operationRule;
	}

	public int getRuleId() {
		return ruleId;
	}

	public void setRuleId(int ruleId) {
		this.ruleId = ruleId;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getOperationRule() {
		return operationRule;
	}

	public void setOperationRule(String operationRule) {
		this.operationRule = operationRule;
	}
	
}
