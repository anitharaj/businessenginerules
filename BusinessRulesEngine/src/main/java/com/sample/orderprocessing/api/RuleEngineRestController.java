package com.sample.orderprocessing.api;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.sample.orderprocessing.models.OrderProcessingRequest;
import com.sample.orderprocessing.models.OrderProcessingResponse;
import com.sample.orderprocessing.models.OrderProcessingRule;
import com.sample.orderprocessing.service.OrderRulesService;
import com.sample.orderprocessing.util.Constants;

@RestController
public class RuleEngineRestController {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass()); 

	@Autowired
	private OrderRulesService orderRulesService;

	@GetMapping(value = Constants.ENDPOINT_GET_RULES)
	public  List<OrderProcessingRule> getAllRules() {
		log.info(Constants.ENDPOINT_GET_RULES + "invoked..");
		return orderRulesService.getAllRules();
	}

	@GetMapping(value = Constants.ENDPOINT_HEARTBEAT)
	public  String getHeartBeat() {
		return "BusinessRulesEngine API is up and running..";
	}

	@PostMapping(value = Constants.ENDPOINT_PROCESS_ORDER) 
	public ResponseEntity<?> postOrderProcessing(@RequestBody OrderProcessingRequest request) 
	{
		log.info(Constants.ENDPOINT_PROCESS_ORDER + "invoked for "+request.getOperation() + " and " + request.getProductType());
		List<OrderProcessingResponse> result = orderRulesService.processOrder(request);
		return ResponseEntity.ok(result); 
	}
	
	@PostMapping(value = Constants.ENDPOINT_ADD_RULE) 
	public ResponseEntity<?> postAddNewRule(@RequestBody OrderProcessingRequest request) 
	{
		log.info(Constants.ENDPOINT_ADD_RULE + "invoked for "+request.getOperation() + " and " + request.getProductType());
		OrderProcessingRule result = orderRulesService.postAddNewRule(request);
		if(result != null)
			log.info("Added new rule succesfully..");
		return ResponseEntity.ok(result); 
	}

}	
