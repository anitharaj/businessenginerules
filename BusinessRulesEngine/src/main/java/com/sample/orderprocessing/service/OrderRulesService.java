package com.sample.orderprocessing.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sample.orderprocessing.db.RulesRepository;
import com.sample.orderprocessing.models.OrderProcessingRequest;
import com.sample.orderprocessing.models.OrderProcessingResponse;
import com.sample.orderprocessing.models.OrderProcessingRule;

@Service
public class OrderRulesService {

	@Autowired
	private RulesRepository rulesRepository;

	public List<OrderProcessingRule> getAllRules(){
		return rulesRepository.findAll();
	}

	public List<OrderProcessingResponse> processOrder(OrderProcessingRequest request) {
		List<OrderProcessingResponse> response = new ArrayList<OrderProcessingResponse>();
		List<OrderProcessingRule> orderRules = null;

		if (request.getOperation().equalsIgnoreCase("payment"))
		{
			orderRules = rulesRepository.findByProductType(request.getProductType());
		}
		response = orderRules.stream().map(orderRule -> mapToResponse(orderRule)).collect(Collectors.toList());
		
		return response;
	}

	private OrderProcessingResponse mapToResponse(OrderProcessingRule orderRule) {
		OrderProcessingResponse resp = new OrderProcessingResponse(orderRule.getProductType(),orderRule.getOperationRule());
		return resp;
	}

	public OrderProcessingRule postAddNewRule(OrderProcessingRequest request) {
		OrderProcessingRule newRule = new OrderProcessingRule(request.getProductType(),request.getOperation());
		return rulesRepository.save(newRule);
	}
}