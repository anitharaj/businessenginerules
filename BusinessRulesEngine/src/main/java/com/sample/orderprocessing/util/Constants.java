package com.sample.orderprocessing.util;

public class Constants {
	
  public final static String ENDPOINT_GET_RULES= "/getallrules";
  public final static String ENDPOINT_HEARTBEAT= "/heartbeat";
  public final static String ENDPOINT_PROCESS_ORDER= "/processorder";
  public static final String ENDPOINT_ADD_RULE = "/addrule";

}
