package com.sample.orderprocessing.util;

public enum PaymentType {
	PHYSICAL,
	BOOK,
	MEMBERSHIP,
	UPGRADE,
	VIDEO
}
