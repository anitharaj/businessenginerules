package com.sample.orderprocessing.db;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sample.orderprocessing.models.OrderProcessingRule;

@Repository
public interface RulesRepository extends JpaRepository<OrderProcessingRule, Serializable> {
    
	List<OrderProcessingRule> findAll();

	List<OrderProcessingRule> findByProductType(String productType);
}
